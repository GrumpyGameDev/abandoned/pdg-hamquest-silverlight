﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace WeightedGeneratorGenerator
{
    class Program
    {
        static int[] Multiply(int[] first, int[] second)
        {
            int[] result = new int[first.Length + second.Length - 1];
            for (int firstIndex = 0; firstIndex < first.Length; ++firstIndex)
            {
                for (int secondIndex = 0; secondIndex < second.Length; ++secondIndex)
                {
                    result[firstIndex + secondIndex] += first[firstIndex] * second[secondIndex];
                }
            }
            return result;
        }
        static XElement GenerateGenerator(int[] die,string name)
        {
            XElement result = new XElement(name);
            result.SetAttributeValue("type", "IntGenerator");
            for (int index = 0; index < die.Length; ++index)
            {
                if (die[index] != 0)
                {
                    XElement entry = new XElement("entry",
                        new XElement("value", index),
                        new XElement("weight", die[index]));
                    result.Add(entry);
                }
            }
            return result;
        }
        static XElement GenerateGenerators(int[] singleDie, int count,int startingIndex,int indexDelta,string nameFormat)
        {
            XElement result = new XElement("generators");
            int[] current = (int[])singleDie.Clone();
            while (count > 0)
            {
                result.Add(GenerateGenerator(current,string.Format(nameFormat,startingIndex)));
                current = Multiply(current, singleDie);
                count--;
                startingIndex+=indexDelta;
            }
            return result;
        }
        static void Main(string[] args)
        {
            XElement generators = GenerateGenerators(new int[] { 2, 1 }, 10,1,1,"white-die-{0}");
            XDocument document = new XDocument(generators);
            document.Save("WhiteDieGenerators.xml");

            generators = GenerateGenerators(new int[] { 1, 1 }, 10,1,1,"red-die-{0}");
            document = new XDocument(generators);
            document.Save("RedDieGenerators.xml");

            generators = GenerateGenerators(new int[] { 1, 2 }, 10, 1, 1, "black-die-{0}");
            document = new XDocument(generators);
            document.Save("BlackDieGenerators.xml");

            generators = GenerateGenerators(new int[] { 0 , 1, 1, 1, 1, 1, 1 }, 10, 1, 1, "cube-die-{0}");
            document = new XDocument(generators);
            document.Save("D6Generators.xml");
        }
    }
}
