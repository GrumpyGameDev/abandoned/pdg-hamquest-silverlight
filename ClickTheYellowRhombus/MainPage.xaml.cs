﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO.IsolatedStorage;
using System.Threading;

namespace ClickTheYellowRhombus
{
    public partial class MainPage : UserControl
    {
        private int highHits = 0;
        private int highMisses = 0;
        private int highScore = 0;
        private int totalHits = 0;
        private int totalMisses = 0;
        private int totalScore = 0;
        private int gamesPlayed = 0;
        private IsolatedStorageSettings userSettings = IsolatedStorageSettings.ApplicationSettings;


        private Random random = new Random();
        private Timer timer;
        private int hits = 0;
        private int misses = 0;
        private int score = 0;
        private int time = 0;

        private void loadHistory()
        {
            try
            {
                highHits = (int)userSettings["highHits"];
                highMisses = (int)userSettings["highMisses"];
                highScore = (int)userSettings["highScore"];
                totalHits = (int)userSettings["totalHits"];
                totalMisses = (int)userSettings["totalMisses"];
                totalScore = (int)userSettings["totalScore"];
                gamesPlayed = (int)userSettings["gamesPlayed"];
            }
            catch
            {
                highHits = 0;
                highMisses = 0;
                highScore = 0;
                totalHits = 0;
                totalMisses = 0;
                totalScore = 0;
                gamesPlayed = 0;
            }
        }

        private void saveHistory()
        {
            userSettings["highHits"]=highHits;
            userSettings["highMisses"]=highMisses;
            userSettings["highScore"]=highScore;
            userSettings["totalHits"]=totalHits;
            userSettings["totalMisses"]=totalMisses;
            userSettings["totalScore"]=totalScore;
            userSettings["gamesPlayed"]=gamesPlayed;
        }

        private void hideHistory()
        {
            historyGrid.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void showHistory()
        {
            historyHighHits.Text = highHits.ToString();
            historyHighMisses.Text = highMisses.ToString();
            historyHighScore.Text = highScore.ToString();
            historyTotalHits.Text = totalHits.ToString();
            historyTotalMisses.Text = totalMisses.ToString();
            historyTotalScore.Text = totalScore.ToString();
            int averageHits = (gamesPlayed > 0) ? (totalHits / gamesPlayed) : (0);
            int averageMisses = (gamesPlayed > 0) ? (totalMisses / gamesPlayed) : (0);
            int averageScore = (gamesPlayed > 0) ? (totalScore / gamesPlayed) : (0);
            historyAverageHits.Text = averageHits.ToString();
            historyAverageMisses.Text = averageMisses.ToString();
            historyAverageScore.Text = averageScore.ToString();
            historyGamesPlayed.Text = gamesPlayed.ToString();
            historyGrid.Visibility = System.Windows.Visibility.Visible;
        }

        private void newGame()
        {
            hits = 0;
            misses = 0;
            score = 0;
            time = 64;
            startButton.IsEnabled = false;
            hideHistory();
            timer.Change(0, 1000);
            updateStats();
        }

        private void updateStats()
        {
            hitText.Text = string.Format("Hit: {0}", hits);
            missText.Text = string.Format("Miss: {0}", misses);
            scoreText.Text = string.Format("Score: {0}", score);
            timeText.Text = string.Format("Time: {0}", time>60?60:time);
        }

        public MainPage()
        {
            InitializeComponent();
            timer = new Timer(this.tick,null,Timeout.Infinite,Timeout.Infinite);
            loadHistory();
            showHistory();
        }

        private void gameOver()
        {
            totalHits += hits;
            totalMisses += misses;
            totalScore += score;
            highHits = (hits > highHits) ? (hits) : (highHits);
            highMisses = (misses > highMisses) ? (misses) : (highMisses);
            highScore = (score > highScore) ? (score) : (highScore);
            gamesPlayed++;
            saveHistory();

            rhombus.Visibility = System.Windows.Visibility.Collapsed;
            showHistory();
            startButton.IsEnabled = true;
            timer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        private void ShowCountDown(int number)
        {
            if (number > 0)
            {
                countDown.Visibility = Visibility.Visible;
                countDown.Text = number.ToString();
            }
            else
            {
                countDown.Visibility = Visibility.Collapsed;
                moveRhombus();
                rhombus.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void tick(object state)
        {
            if (time > 0)
            {
                time--;
                switch (time)
                {
                    case 63:
                    case 62:
                    case 61:
                    case 60:
                        Dispatcher.BeginInvoke(()=>ShowCountDown(time - 60));
                        break;
                }
            }
            if (time <= 0)
            {
                Dispatcher.BeginInvoke(() => gameOver());
            }
            Dispatcher.BeginInvoke(() => updateStats());
        }

        private void moveRhombus()
        {
            Canvas.SetLeft(rhombus, random.Next((int)field.Width-(int)rhombus.Width) + Canvas.GetLeft(field));
            Canvas.SetTop(rhombus, random.Next((int)field.Height-(int)rhombus.Height) + Canvas.GetTop(field));
        }

        private void rhombus_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (time > 0 && time<=60)
            {
                hits++;
                score += 10;
                moveRhombus();
                updateStats();
            }
        }

        private void field_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (time > 0 && time <= 60)
            {
                misses++;
                if (score > 15)
                {
                    score -= 15;
                }
                else
                {
                    score = 0;
                }
                moveRhombus();
                updateStats();
            }
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            if (time == 0)
            {
                newGame();
            }
        }
    }
}
