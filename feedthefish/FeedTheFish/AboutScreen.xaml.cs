﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace FeedTheFish
{
    public partial class AboutScreen : UserControl
    {
        public AboutScreen()
        {
            InitializeComponent();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            MainPage.Singleton.aboutScreen.Visibility = Visibility.Collapsed;
            MainPage.Singleton.titleScreen.Visibility = Visibility.Visible;
        }
    }
}
