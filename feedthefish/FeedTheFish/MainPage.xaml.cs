﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace FeedTheFish
{
    public partial class MainPage : PhoneApplicationPage
    {
        private static MainPage mainPage;
        public static MainPage Singleton
        {
            get
            {
                return mainPage;
            }
        }

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            mainPage = this;
        }


    }
}
