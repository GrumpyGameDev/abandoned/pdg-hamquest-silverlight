﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace FeedTheFish
{
    public partial class TitleScreen : UserControl
    {
        public TitleScreen()
        {
            InitializeComponent();
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void InstructionsButton_Click(object sender, RoutedEventArgs e)
        {
            MainPage.Singleton.titleScreen.Visibility = Visibility.Collapsed;
            MainPage.Singleton.instructionsScreen.Visibility = Visibility.Visible;
        }

        private void StatisticsButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            MainPage.Singleton.titleScreen.Visibility = Visibility.Collapsed;
            MainPage.Singleton.aboutScreen.Visibility = Visibility.Visible;
        }

    }
}
