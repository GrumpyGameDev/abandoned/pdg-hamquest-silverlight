function Directions(){
	var opposites = [2,3,0,1];
	var deltaX = [0,1,0,-1];
	var deltaY = [-1,0,1,0];
	this.getFirst = function(){return 0;};
	this.getLast = function(){return 3;};
	this.getNext = function(theDirection){return (theDirection+1)%4;};
	this.getPrevious = function(theDirection){return (theDirection+3)%4;};
	this.getValues = function(){return [0,1,2,3];};
	this.getOpposite = function(theDirection){return opposites[theDirection%4];};
	this.getNextColumn = function(theStartColumn,theStartRow,theDirection){return theStartColumn+deltaX[theDirection%4];};
	this.getNextRow = function(theStartColumn,theStartRow,theDirection){return theStartRow+deltaY[theDirection%4];};
}