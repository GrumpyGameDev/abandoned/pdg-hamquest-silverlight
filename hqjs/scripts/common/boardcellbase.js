function BoardCellBase(theParent,theColumn,theRow){
	this.base = NestedClass;
	this.base(theParent);
	delete this.base;
	
	var row = theRow;
	var column = theColumn;
	this.getRow = function(){
		return row;
	};
	this.getColumn = function(){
		return column;
	};
	this.clear = function(){
	};
}