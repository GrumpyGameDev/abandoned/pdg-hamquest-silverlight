function Board(theParent,theColumns,theRows,theCellType){
	this.base = NestedClass;
	this.base(theParent);
	delete this.base;
	var columns = [];
	this.getColumns = function(){
		return columns.length;
	};
	this.getRows = function(){
		return this.getColumn(0).getRows();
	};
	this.getColumn = function(theColumn){
		if(theColumn>=0 && theColumn<this.getColumns()){
			return columns[theColumn];
		}else{
			return null;
		}
	};
	this.clear = function(){
		for(var index in columns)[
			columns[index].clear();
		}
	};
	while(columns.length<theColumns){
		var theColumn = new BoardColumn(this,columns.length,theRows,theCellType);
		columns.push(theColumn);
	}
}