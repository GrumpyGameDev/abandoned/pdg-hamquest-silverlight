function WeightedGenerator(){
	var theCollection={};
	var weightTotal = 0;
	this.clear = function(){
		theCollection = {};
	};
	this.setWeight = function(theValue,theWeight){
		var theKey = JSON.stringify(theValue);
		weightTotal -= this.getWeight(theValue);
		if(theWeight>0){
			theCollection[theKey] = theWeight;
		}else{
			delete theCollection[theKey];
		}
		weightTotal += this.getWeight(theValue);
	};
	this.getWeight = function(theValue){
		var theKey = JSON.stringify(theValue);
		var theWeight = theCollection[theKey];
		if(theWeight==null) theWeight = 0;
		return theWeight;
	};
	this.generate = function(){
		var theRandomValue = RandomNumberGenerator.next(weightTotal);
		for(var key in theCollection){
			var theWeight = theCollection[key];
			if(theRandomValue<theWeight){
				return JSON.parse(key);
			}else{
				theRandomValue -= theWeight;
			}
		}
		return null;
	};
	var defaultComparisonFunction = function (first,second){
		if(first==second) return 0;
		if(first<second) return -1;
		return 1;
	};
	this.getMinimalValue = function(theComparisonFunction){
		if(theComparisonFunction==null) theComparisonFunction = defaultComparisonFunction;
		var theResult = null;
		for(var key in theCollection){
			if(theResult==null || theComparisonFunction(key,theResult)==-1){
				theResult = key;
			}
		}
		return JSON.parse(theResult);
	};
	this.getMaximalValue = function(theComparisonFunction){
		if(theComparisonFunction==null) theComparisonFunction = defaultComparisonFunction;
		var theResult = null;
		for(var key in theCollection){
			if(theResult==null || theComparisonFunction(key,theResult)==1){
				theResult = key;
			}
		}
		return JSON.parse(theResult);
	};
}
