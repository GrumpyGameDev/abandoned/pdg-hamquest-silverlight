var RandomNumberGenerator = {
	"next":function(minimum,maximum){
		if(maximum==null){
			if(minimum==null){
				return 0;
			}else{
				return this.next(0,minimum);
			}
		}else{
			return Math.floor(Math.random() * (maximum - minimum)) + minimum;
		}
	}
};