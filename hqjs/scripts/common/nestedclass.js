function NestedClass(theParent){
	var parent = theParent;
	this.getParent = function(){
		return parent;
	}
	var clearFunctions = [];
	this.addClearFunction = function(theClearFunction){
		clearFunctions.push(theClearFunction);
	}
}