function MazeCellBase(theParent,theColumn,theRow,theDirectionsType,theCellInfoType){
	this.base = BoardCellBase;
	this.base(theParent,theColumn,theRow);
	delete this.base;
	var neighbors = [];
	var portals = [];
	var cellInfo = new theCellInfoType(this);
	var directions = new theDirectionsType();
	for(var index in directions.getValues()){
		neighbors.push(null);
		portals.push(null);
	}
	this.getNeighbor = function(theDirection){
		if(theDirection>=0 && theDirection<neighbors.length){
			return neighbors[theDirection];
		}else{
			return null;
		}
	};
	this.setNeighbor = function(theDirection,theNeighbor){
		if(theDirection>=0 && theDirection<neighbors.length){
			neighbors[theDirection]=theNeighbor;
		}
	};
	this.getPortal = function(theDirection){
		if(theDirection>=0 && theDirection<portals.length){
			return portals[theDirection];
		}else{
			return null;
		}
	};
	this.setPortal = function(theDirection,thePortal){
		if(theDirection>=0 && theDirection<portals.length){
			portals[theDirection]=theNeighbor;
		}
	};
	this.getOpenPortalCount = function(){
		var result = 0;
		for(var index in directions.getValues()){
			if(this.getPortal(index)!=null && this.getPortal(index).getOpen()){
				result++;
			}
		}
		return result;
	};
	this.getCellInfo = function(){
		return cellInfo;
	};
	var oldClear = this.clear;
	this.clear = function(){
		if(oldClear!=null){
			var currentClear = this.clear;
			this.clear = oldClear;
			this.clear();
			this.clear = currentClear;
		}
		cellInfo.clear();
	};
}