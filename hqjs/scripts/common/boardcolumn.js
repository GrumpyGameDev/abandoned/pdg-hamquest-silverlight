function BoardColumn(theParent,theColumn,theRows,theCellType){
	this.base = NestedClass;
	this.base(theParent);
	delete this.base;
	var column = theColumn;
	var cells = [];
	this.getColumn = function(){
		return column;
	}
	this.getRows=function(){
		return cells.length;
	};
	this.getRow = function(theRow){
		if(theRow>=0 && theRow<this.getRows()){
			return cells[theRow];
		}else{
			return null;
		}
	};
	this.clear = function(){
		for(var index in cells){
			cells[index].clear();
		}
	};
	while(this.getRows()<theRows){
		var theCell = new theCellType(this,theColumn,this.getRows());
		cells.push(theCell);
	}
}