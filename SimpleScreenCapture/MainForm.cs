﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SimpleScreenCapture
{
    public partial class MainForm : Form
    {
        private FrameForm frameForm;

        public MainForm()
        {
            InitializeComponent();
            this.TopMost = true;
            frameForm = new FrameForm();
            frameForm.Visible = false;
        }

        private void ErrorMessage(string message)
        {
            MessageBox.Show(message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnCapture_Click(object sender, EventArgs e)
        {
            int left;
            int top;
            int width;
            int height;
            string path;
            int number;

            if (!int.TryParse(txtLeft.Text, out left))
            {
                ErrorMessage("Left");
                return;
            }
            if (!int.TryParse(txtTop.Text, out top))
            {
                ErrorMessage("Top");
                return;
            }
            if (!int.TryParse(txtWidth.Text, out width))
            {
                ErrorMessage("Width");
                return;
            }
            if (!int.TryParse(txtHeight.Text, out height))
            {
                ErrorMessage("Height");
                return;
            }
            if (!int.TryParse(txtNumber.Text, out number))
            {
                ErrorMessage("Number");
                return;
            }
            path = txtPath.Text;

            string completefilename = string.Format(path, number);

            ScreenCapture sc = new ScreenCapture();
            Image img = sc.CaptureScreen();
            Bitmap bmp = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(bmp);
            g.DrawImage(img, new Rectangle(0,0,width,height), new Rectangle(left,top,width,height), GraphicsUnit.Pixel);

            bmp.Save(completefilename, System.Drawing.Imaging.ImageFormat.Png);
            number++;
            txtNumber.Text = number.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int left;
            int top;
            int width;
            int height;

            if (!int.TryParse(txtLeft.Text, out left))
            {
                ErrorMessage("Left");
                return;
            }
            if (!int.TryParse(txtTop.Text, out top))
            {
                ErrorMessage("Top");
                return;
            }
            if (!int.TryParse(txtWidth.Text, out width))
            {
                ErrorMessage("Width");
                return;
            }
            if (!int.TryParse(txtHeight.Text, out height))
            {
                ErrorMessage("Height");
                return;
            }

            frameForm.Visible = !frameForm.Visible;
            frameForm.Left = left - 4;
            frameForm.Top = top - 4;
            frameForm.Width = width + 8;
            frameForm.Height = height + 8;

            Region region = new Region(new Rectangle(0, 0, width + 8, height + 8));
            region.Exclude(new Rectangle(4, 4, width, height));
            frameForm.Region = region;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtPath.Text = saveFileDialog1.FileName;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                frameForm.BackColor = colorDialog1.Color;
            }
        }

    }
}
