﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WLMXTool
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            cmbSelection.SelectedIndex = 0;
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            switch (cmbSelection.SelectedIndex)
            {
                case 1:
                    doFadeDialog();
                    break;
                default:
                    MessageBox.Show("Please select a task from the drop down list.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
        }

        private void doFadeDialog()
        {
            FadeEffectDialog dialog = new FadeEffectDialog();
            dialog.ShowDialog();
        }

    }
}
