﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace WLMXTool
{
    public partial class FadeEffectDialog : Form
    {
        public FadeEffectDialog()
        {
            InitializeComponent();
        }

        private void btnColor_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                pbColor.BackColor = colorDialog.Color;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            string sourceFile = string.Empty;
            switch (cmbInOut.SelectedIndex)
            {
                case 0:
                    sourceFile = Directory.GetCurrentDirectory()+"\\Templates\\FadeInFromXXXXEffectTemplate.wlmx";
                    break;
                case 1:
                    sourceFile = Directory.GetCurrentDirectory() + "\\Templates\\FadeOutToXXXXEffectTemplate.wlmx";
                    break;
            }

            string id = txtId.Text;
            string title = txtTitle.Text;
            string filename = txtFilename.Text;
            Color color = pbColor.BackColor;
            float red = (float)color.R/255.0f;
            float green = (float)color.G / 255.0f;
            float blue = (float)color.B / 255.0f;
            float alpha = 1.0f;

            string content = string.Empty;
            using (TextReader reader = File.OpenText(sourceFile))
            {
                content = reader.ReadToEnd();
                reader.Close();
            }

            content = content.Replace("{{{TemplateID}}}", id);
            content = content.Replace("{{{TemplateTitle}}}", title);
            content = content.Replace("{{{Red}}}", red.ToString());
            content = content.Replace("{{{Green}}}", green.ToString());
            content = content.Replace("{{{Blue}}}", blue.ToString());
            content = content.Replace("{{{Alpha}}}", alpha.ToString());

            using (TextWriter writer = File.CreateText(filename))
            {
                writer.Write(content);
                writer.Close();
            }

        }

        private void btnFilename_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                txtFilename.Text = saveFileDialog.FileName;
            }

        }

        private void FadeEffectDialog_Load(object sender, EventArgs e)
        {
            cmbInOut.SelectedIndex = 0;
        }

        private void cmbInOut_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cmbInOut.SelectedIndex)
            {
                case 0:
                    txtId.Text = "FadeInFromXXXXEffectTemplate";
                    txtTitle.Text = "Fade in from xxxx";
                    break;
                case 1:
                    txtId.Text = "FadeOutToXXXXEffectTemplate";
                    txtTitle.Text = "Fade out to xxxx";
                    break;
            }
        }

    }
}
